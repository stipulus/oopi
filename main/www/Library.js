var Vehicle = oopi.abstract({
    construct: function (a) {
        console.log('parent',a);
    },
    color: 'blue'
});

var Car = Vehicle.prototype.extend({
    sayText: 'hi',
    construct: function (a,b,c) {
        console.log(a);
        console.log(b);
        console.log(c);
        this.setText('hello');
    },
    say: function () {
        console.log('say: '+this.sayText);
    },
    setText: function (v) {
        this.sayText = v;
    }
});

var SadCar = Car.prototype.extend({
    construct: function (a) {
        console.log('child',a);
    },
    say: function () {
        console.log(this.color+' :(');
    }
});